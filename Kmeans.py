import pandas as pd
from sklearn.metrics import silhouette_score
from RecuperationDonne import dataframe
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder

chemin_fichier = 'Ressources/resulta.xls'

df = dataframe(chemin_fichier)

df = df.drop(['Departements', 'ID'], axis=1) 

categorical_columns = df.select_dtypes(include=['object']).columns

encoder = OneHotEncoder(sparse=False)
encoded_columns = encoder.fit_transform(df[categorical_columns])

encoded_df = pd.DataFrame(encoded_columns, columns=encoder.get_feature_names_out(categorical_columns))

df = df.drop(categorical_columns, axis=1)
df = pd.concat([df, encoded_df], axis=1)


scaler = StandardScaler()
features_scaled = scaler.fit_transform(df)

kmeans = KMeans(n_clusters=3 , n_init=10, random_state=42)
kmeans.fit(features_scaled)

df['Cluster'] = kmeans.labels_
silhouette = silhouette_score(df, df['Cluster'])
print(df.dtypes)


cluster_target_relation = df.groupby('Cluster')['Target_Centre'].mean()

print(f'Silhouette Score: {silhouette}')

print(cluster_target_relation)
