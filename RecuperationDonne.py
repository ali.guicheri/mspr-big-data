from numpy import int64
import pandas as pd

def dataframe (chemin_fichier):
    df = pd.read_excel(chemin_fichier)
    df["_0_19_ans"] = df["_0_19_ans"].str.replace('\xa0', '')
    df["_0_19_ans"] = df["_0_19_ans"].astype(int64)
    df["_20_39_ans"] = df["_20_39_ans"].str.replace('\xa0', '')
    df["_20_39_ans"] = df["_20_39_ans"].astype(int64)
    df["_40_59_ans"] = df["_40_59_ans"].str.replace('\xa0', '')
    df["_40_59_ans"] = df["_40_59_ans"].astype(int64)
    df["_60_74_ans"] = df["_60_74_ans"].str.replace('\xa0', '')
    df["_60_74_ans"] = df["_60_74_ans"].astype(int64)
    df["_75_et_plus"] = df["_75_et_plus"].str.replace('\xa0', '')
    df["_75_et_plus"] = df["_75_et_plus"].astype(int64)
    df["total"] = df["total"].str.replace('\xa0', '')
    df["total"] = df["total"].astype(int64)
    df["Total_Condamnes"] = df["Total_Condamnes"].str.replace(' ', '')
    df["Total_Condamnes"] = df["Total_Condamnes"].astype(int64)
    df["Salaire_net_horaire_moyen_en_2021"] = df["Salaire_net_horaire_moyen_en_2021"].str.replace(',', '.')
    df["Salaire_net_horaire_moyen_en_2021"] = df["Salaire_net_horaire_moyen_en_2021"].astype('float64')
    print(df.dtypes)
    print(df.head())
    return df
