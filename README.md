# Installer le projet

- python -m venv mspr
- .\mspr\Scripts\activate
- pip install pandas
- pip install openyxl
- pip install xlrd
- pip install scikit-learn

# Run le projet 

- python nomDuFicher.py

# Consigne à suivre

- si vous etes sur un nouveau model de machine learning creer un nouveau ficher
- ne pas toucher au fichier RecuperationDonne.py